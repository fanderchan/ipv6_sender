# ipv6_sender

#### 介绍
一个叫 ipv6_sender.py 的 python3 脚本，用于检查你的 IPV6 地址是否发生变化，如果变化则发送给你的 QQ 邮箱。 

#### 使用说明
1. QQ 邮箱上配置 smtp 功能\
略

2. 脚本配置
```
# 设置邮件参数
sender = 'xxxxxx@qq.com'  # 发件人邮箱地址，建议发件人和收件人一个地址即可
password = 'gxulhidzypkabcde'  # 发件人邮箱授权码
receiver = 'xxxxxx@qq.com'  # 收件人邮箱地址，建议发件人和收件人一个地址即可
```
3. 运行
```
python3 ipv6_sender.py
```

#### 使用限制
只支持 windows 系统。

# update_ipv6_dns.py

配合 windows 或 linux 的定时任务，定期把 ipv6 地址更新到 duckdns 或 dynadot，实现 DDNS 功能。\
Linux的/etc/crontab加入如下
```
*/5 * * * * root /usr/bin/python3 /opt/update_ipv6_dns.py >> /opt/update_ipv6_dns.log 2>/dev/null
```